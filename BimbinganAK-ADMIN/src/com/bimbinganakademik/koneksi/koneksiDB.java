
package com.bimbinganakademik.koneksi;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.Statement;

public class koneksiDB {
    
    private static Connection koneksi = null;
    private static Statement perintah = null;

    public static Connection bukaKoneksi(){
         if(koneksi == null){
            String url = "jdbc:mysql://localhost:3306/bimbinganak";
            String user = "root";
            String pass = "";
            try {
                Class.forName("com.mysql.jdbc.Driver");
                koneksi = DriverManager.getConnection(url, user, pass);
                perintah = koneksi.createStatement();
                System.out.println("Anda terhubung");
            } 
            catch(Exception error){
                System.out.println("Anda terputus");
            }
        }
        return koneksi;
    }
    
    public static Connection tutupKoneksi(){
        if(koneksi != null) {
            try {
                koneksi.close();
            }
            catch(Exception e) {
                System.out.println("Gagal menutup koneksi");
            }
        }
        return koneksi;
    }
}
